import React, { useReducer } from "react";
import MyContext from "./store";
import {listReducer} from '../reducer/todoReducer';


export const Provider = ({ children }) => {
    const [state, dispatch] = useReducer(listReducer);
  
    return (
      <MyContext.Provider value={{ state, dispatch }}>{children}</MyContext.Provider>
    );
  };