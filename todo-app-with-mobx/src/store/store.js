import {
    action,
    makeObservable,
    observable,
    autorun,
  } from "mobx";
  
  class ToDoStore {
    list = [];
    item = [];
  
    constructor() {
      makeObservable(this, {
        list: observable,
        item: observable,
        createList: action,
        updateList: action,
        deleteList: action,
        clearList:action
      });
      autorun(this.logStoreDetails);
    //   runInAction(this.prefetchData);
    }
  
  
    createList(data = { id: 0, title: "" }) {
      if(this.item.length === 0)  this.item.push(data);
      else this.list.push(data);
    }
  
  
    updateList(data) {
      const itemIndexAtId = this.list.findIndex((item) => item.id === data.id);
      if (itemIndexAtId > -1) {
        this.list[itemIndexAtId] = data;
      }
    }
  
    deleteList(listId) {
      const newList = this.list.filter((item) => item.id !== listId); 
        this.list = newList;
    }

    clearList() {
        this.list = [];
    }
  
    logStoreDetails = () => {
      console.log(this.list);
    };
  }

  const store = new ToDoStore();
  
  export default store;