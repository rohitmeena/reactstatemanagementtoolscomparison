import React, { useState } from "react";
import List from "./components/List";
import Alert from "./components/Alert";
import "./App.css";
import { observer } from "mobx-react-lite";


const Comp1 = observer(({items}) => {
return (
  <div>
  demo component one {items.length}
  </div>
)
})
const Comp2 = observer(({items}) => {
  return (
    <div>
    demo component two {items.length}
    </div>
  )
  })


function App(props) {
  const { store } = props;
  const { list, item } =  store;
  const [name, setName] = useState("");
  const [isEditing, setIsEditing] = useState(false);
  const [editID, setEditID] = useState(null);
  const [alert, setAlert] = useState({ show: false, msg: "", type: "" });


  const handleSubmit = (e) => {
    e.preventDefault();
    if (!name) {
      showAlert(true, "danger", "please enter value");
    } else if (name && isEditing) {
      setName("");
      setEditID(null);
      setIsEditing(false);
      showAlert(true, "success", "value changed");
      store.updateList({id:editID, title: name});
    } else {
      showAlert(true, "success", "item added to the list");
      store.createList({ id: Date.now(), title: name });
      setName("");
    }
  };

  
  const showAlert = (show = false, type = "", msg = "") => {
    setAlert({ show, type, msg });
  };
  
  const clearList = () => {
    showAlert(true, "danger", "empty list");
    store.clearList();
  };

  const removeItem = (id) => {
    showAlert(true, "danger", "item removed");
    store.deleteList(id);
  };

  const editItem = (id) => {
    const specificItem = list.find((item) => item.id === id);
    setIsEditing(true);
    setEditID(id);
    setName(specificItem.title);
  };

//  console.log('lists=>',Array.from(store.list), 'items=>',Array.from(store.item));


  return (
    <section className="section-center">
      <form className="grocery-form" onSubmit={handleSubmit}>
        {alert.show && <Alert {...alert} removeAlert={showAlert} list={list} />}

        <h3>Todo App</h3>
        <div className="form-control">
          <input type="text" className="grocery" placeholder="Input your Todo List" value={name} onChange={(e) => setName(e.target.value)} />
          <button type="submit" className="submit-btn">
            {isEditing ? "edit" : "Submit!"}
          </button>
        </div>
      </form>
      {list.length > 0 && (
        <div className="grocery-container">
          <List items={list} removeItem={removeItem} editItem={editItem} />
          {/* <Comp1 items={list} />
          <Comp2 items={item} /> */}
          <button className="clear-btn" onClick={clearList}>
            clear items
          </button>
        </div>
      )}
    </section>
  );
}

export default observer(App);
// export default App;
