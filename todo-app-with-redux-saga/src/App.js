import React, { useState } from "react";
import List from "./components/List";
import Alert from "./components/Alert";
import "./App.css";
import { useSelector, useDispatch } from 'react-redux';
import {createList, updateList, clearList, deleteList} from './actions/todoActions';


function App() {
  // using hooks to connect redux state and dispatch method instead of connect HOC
  const { list } = useSelector(state=> state.todoList);
  const dispatch = useDispatch();

  const [name, setName] = useState("");
  const [isEditing, setIsEditing] = useState(false);
  const [editID, setEditID] = useState(null);
  const [alert, setAlert] = useState({ show: false, msg: "", type: "" });


  const handleSubmit = (e) => {
    e.preventDefault();
    if (!name) {
      showAlert(true, "danger", "please enter value");
    } else if (name && isEditing) {
      setName("");
      setEditID(null);
      setIsEditing(false);
      showAlert(true, "success", "value changed");
      dispatch(updateList({id:editID, title: name}));
    } else {
      showAlert(true, "success", "item added to the list");
      dispatch(createList({ id: Date.now(), title: name }));
      setName("");
    }
  };

  
  const showAlert = (show = false, type = "", msg = "") => {
    setAlert({ show, type, msg });
  };
  
  const clearListItems = () => {
    showAlert(true, "danger", "empty list");
    dispatch(clearList());
  };

  const removeItem = (id) => {
    showAlert(true, "danger", "item removed");
    dispatch(deleteList(id));
  };

  const editItem = (id) => {
    const specificItem = list.find((item) => item.id === id);
    setIsEditing(true);
    setEditID(id);
    setName(specificItem.title);
  };


  return (
    <section className="section-center">
      <form className="grocery-form" onSubmit={handleSubmit}>
        {alert.show && <Alert {...alert} removeAlert={showAlert} list={list} />}

        <h3>Todo App</h3>
        <div className="form-control">
          <input type="text" className="grocery" placeholder="Input your Todo List" value={name} onChange={(e) => setName(e.target.value)} />
          <button type="submit" className="submit-btn">
            {isEditing ? "edit" : "Submit!"}
          </button>
        </div>
      </form>
      {list.length > 0 && (
        <div className="grocery-container">
          <List items={list} removeItem={removeItem} editItem={editItem} />
          <button className="clear-btn" onClick={clearListItems}>
            clear items
          </button>
        </div>
      )}
    </section>
  );
}


export default App;

