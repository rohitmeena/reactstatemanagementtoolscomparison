export const createList = (payload) => ({type:'ADD_ITEM', payload});

export const updateList = (payload) => ({type:'UPDATE_ITEM', payload});

export const deleteList = (payload) => ({type:'DELETE_ITEM', payload});

export const clearList = () => ({type:'CLEAR_ITEM'});

// saga
export const createListSaga = (payload) => ({type:'ADD_ITEM_SAGA', payload});

export const updateListSaga = (payload) => ({type:'UPDATE_ITEM_SAGA', payload});

export const deleteListSaga = (payload) => ({type:'DELETE_ITEM_SAGA', payload});

export const clearListSaga = () => ({type:'CLEAR_ITEM_SAGA'});