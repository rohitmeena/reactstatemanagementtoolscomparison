import { call, put, all, takeLatest } from 'redux-saga/effects';
import { createListSaga, deleteListSaga, updateListSaga, clearListSaga } from '../actions/todoActions';
import types from '../actions/types';


// saga catch the fired actions as a middleware then dispatch to the reducer
// saga
export function* onAdd(){
  yield takeLatest(types.ADD_ITEM, onAddItemSaga);
}

export function* onDelete(){
  yield takeLatest(types.DELETE_ITEM, onDeleteItemSaga);
}

export function* onUpdate(){
  yield takeLatest(types.UPDATE_ITEM, onUpdateItemSaga);
}

export function* onClear(){
  yield takeLatest(types.CLEAR_ITEM, onClearItemSaga);
}

// watcher
export function* onAddItemSaga({payload}) {
  yield put(createListSaga(payload));
}

export function* onDeleteItemSaga({payload}) {
  yield put(deleteListSaga(payload));
}

export function* onUpdateItemSaga({payload}) {
  yield put(updateListSaga(payload));
}

export function* onClearItemSaga({payload}) {
  yield put(clearListSaga(payload));
}


export default function* todoItems(){
  yield all([call(onDelete), call(onAdd), call(onUpdate), call(onClear)]);
}