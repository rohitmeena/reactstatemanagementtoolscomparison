import { combineReducers } from 'redux';
import  listReducer from './todoReducer';


export default combineReducers({
    todoList:listReducer,
});