import types from '../actions/types';

const initialState = {
    list : []
}


const listReducer = (state=initialState, action) => {
    let newList;

    switch(action.type){

        case types.ADD_ITEM_SAGA : 
            return {...state, list : [...state.list, action.payload] }

        case types.UPDATE_ITEM_SAGA : 
            newList = [...state.list];
            const itemIndexAtId = newList.findIndex((item) => item.id === action.payload.id);
            if (itemIndexAtId > -1) {
              newList[itemIndexAtId] = action.payload;
            }
            return {...state, list : newList } 

        case  types.DELETE_ITEM_SAGA :
            newList = [...state.list];
            newList = newList.filter((item) => item.id !== action.payload); 
            return {...state, list : newList } 

        case types.CLEAR_ITEM_SAGA:
            return {...state, list : []} 

        default:
            return state; 
               
    }
}


export default listReducer;