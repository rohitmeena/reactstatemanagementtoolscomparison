import types from '../actions/types';

export const createList = (payload) => ({type:types.ADD_ITEM, payload});

export const updateList = (payload) => ({type:types.UPDATE_ITEM, payload});

export const deleteList = (payload) => ({type:types.DELETE_ITEM, payload});

export const clearList = () => ({type:types.CLEAR_ITEM});