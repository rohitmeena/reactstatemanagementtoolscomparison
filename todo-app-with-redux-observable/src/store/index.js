import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import rootReducer from '../reducers';
import rootEpics from '../epics';


const epicMiddleware = createEpicMiddleware();
// use applyMiddleware to add the thunk middleware to the store
const store = createStore(rootReducer, applyMiddleware(epicMiddleware));
epicMiddleware.run(rootEpics);

export default store;
