import { combineEpics, ofType } from 'redux-observable';
import { map, delay } from 'rxjs/operators';
import types from '../actions/types';

// basically used for async programming 
// it uses rxjs observable 
// When an Epic receives an action, it has already been run through your reducers and the state updated.
const createListEpic = action$ => action$.pipe(
    ofType(types.ADD_ITEM),
    delay(1000),
    map(action=>({ type: types.ADD_ITEM_EP, payload:action.payload }))
  );


export default combineEpics(
    createListEpic
);